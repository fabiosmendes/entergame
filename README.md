Para rodar teste:
clonar repositório ( git clone git@bitbucket.org:fabiosmendes/silexapp.git )

executar composer na raiz

configurar banco de dados (app/config/doctrine.php)

executar comando vendor/bin/doctrine orm:schema-tool:create

executar server apontado para pasta public/ . Ex: php -S http://localhost:8080 -t public

acessar no navegador a url http://localhost:8080/clients/load

acessar no navegador a url http://localhost:8080/client01

O exemplo é simples e não possui um template. Algumas funcionalidades não está habilitadas (é um teste para um modelo básico de API).