<?php

namespace EnterGame;

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\TwigServiceProvider;

use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;

use EnterGame\Controllers\Provider\Home;
use EnterGame\Controllers\Provider\Game;
use EnterGame\Controllers\Provider\Player;
use EnterGame\Controllers\Provider\Score;

class Config
{
    protected $app;

    public function init(Application $app)
    {
        $app['debug'] = true; // dev mode

        $configDoctrine = include __DIR__.'/../../app/config/doctrine.php';

        $app->register(new DoctrineServiceProvider, $configDoctrine['db.options']);
        $app->register(new DoctrineOrmServiceProvider, $configDoctrine);

        $app->register(new ServiceControllerServiceProvider());

        $app->register(new TwigServiceProvider(), [ // @todo - remove twig?
            'twig.path' => __DIR__.'/../../../views',
        ]);

        /**
         * Mount Controllers
         */
        $app->mount('home', new Home());
        $app->mount('games', new Game());
        $app->mount('players', new Player());
        $app->mount('score', new Score());
    }
}