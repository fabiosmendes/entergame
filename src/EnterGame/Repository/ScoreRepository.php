<?php

namespace EnterGame\Repository;

use Doctrine\ORM\EntityRepository;

class ScoreRepository extends EntityRepository
{
    public function getAll($limit = 10)
    {
        $sqlScores = "  SELECT p.nickname, p.avatar, ";
        $sqlScores .= "  p.description, s.points";
        $sqlScores .= " FROM scores s, players p ";
        $sqlScores .= " WHERE s.player_id = p.id";
        $sqlScores .= " ORDER BY s.points DESC";
        $sqlScores .= " LIMIT ".$limit;

        $stmt = $this->executeSql($sqlScores);
        $result = $stmt->fetchAll();

        return $result;
    }

    public function getGameScore($id, $limit = 10)
    {
        $sqlScores = "  SELECT p.nickname, p.avatar, ";
        $sqlScores .= "  p.description, s.points";
        $sqlScores .= " FROM scores s, players p ";
        $sqlScores .= " WHERE s.player_id = p.id";
        $sqlScores .= " AND p.game_id = " . $id;
        $sqlScores .= " ORDER BY s.points DESC";
        $sqlScores .= " LIMIT ".$limit;

        $stmt = $this->executeSql($sqlScores);
        $result = $stmt->fetchAll();

        return $result;
    }

    public function executeSql($sql)
    {
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        try {
            $stmt->execute();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return $stmt;
    }
}