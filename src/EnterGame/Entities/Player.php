<?php

namespace EnterGame\Entities;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="players")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="EnterGame\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var integer
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=70, nullable=false)
     */
    private $nickname;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=70, nullable=false)
     */
    private $avatar;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=70, nullable=false)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="players")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     */
    private $game;

    /**
     * @ORM\OneToMany(targetEntity="Score", mappedBy="player")
     */
    private $scores;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    public function __construct()
    {
        $this->scores = new ArrayCollection();

        $this->created = new \DateTime("now");
        $this->modified = new \DateTime("now");
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Player
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     * @return Player
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return Player
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Player
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     *
     * @return \EnterGame\Entities\Game $game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     *
     * @param \EnterGame\Entities\Game $game
     * @return \EnterGame\Entities\Player
     */
    public function setGame($game)
    {
        $this->game = $game;
        return $this;
    }

    /**
     *
     * @param \EnterGame\Entities\Score $score
     * @return $this
     */
    public function addScore(Score $score)
    {
        $this->scores[] = $score;
        return $this;
    }

    /**
     *
     * @param \EnterGame\Entities\Score $score
     */
    public function removeScore(Score $score)
    {
        $this->scores->removeElement($score);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScores()
    {
        return $this->scores;
    }



    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return Player
     */
    public function setCreated()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @ORM\PreUpdate
     *
     * @return Player
     */
    public function setModified()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }

    public function __toString()
    {
       return $this->nickname;
    }

    public function __toArray()
    {
        return [
            'id' => $this->id,
            'nickname' => $this->nickname,
            'avatar' => $this->avatar,
        ];
    }
}