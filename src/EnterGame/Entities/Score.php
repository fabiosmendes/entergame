<?php

namespace EnterGame\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="scores")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="EnterGame\Repository\ScoreRepository")
 */
class Score
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="scores")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     */
    protected $player;


    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=true, options={"comment" = ""})
     */
    private $points;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    public function __construct()
    {
        $this->points = 0;

        $this->created = new \DateTime("now");
        $this->modified = new \DateTime("now");
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Score
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param mixed $player
     * @return Score
     */
    public function setPlayer($player)
    {
        $this->player = $player;
        $player->addScore($this);
        return $this;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param int $points
     * @return Score
     */
    public function setPoints($points)
    {
        $this->points = $points;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     *
     * @return Score
     */
    public function setCreated()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @return Score
     */
    public function setModified()
    {
        $this->modified = new \DateTime("now");
        return $this;
    }

}