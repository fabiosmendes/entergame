<?php

namespace EnterGame\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;

class HomeController extends BaseController
{
    public function index()
    {
        $list = $this->service->getList();
        return new JsonResponse($list);
    }

    public function load()
    {
        $em = $this->service->getEm();
        var_dump($em); exit();
    }
}