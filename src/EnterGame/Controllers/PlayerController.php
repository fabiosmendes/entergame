<?php

namespace EnterGame\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;

class PlayerController extends BaseController
{
    public function index()
    {
        $list = $this->service->getList();
        return new JsonResponse($list);
    }

    public function show($id)
    {
        $entity = $this->service->getItem($id);
        if($entity) {
            $return = [
                'object'  => $entity->__toArray(),
                'message' => '',
                'status'  => true
            ];
            return new JsonResponse($return);
        }

        $return = [
            'object'  => 'null',
            'message' => $this->service->getMessage(),
            'status'  => false
        ];

        return new JsonResponse($return, 400);
    }

    public function store()
    {
        $data = $this->getParameters();

        $game = [
            'nickname' => $data['nickname'],
            'avatar' => $data['avatar'], // @todo check upload via api
            'description' => $data['description'],
            'game' => $data['game']
        ];

        $status = $this->service->newRegister($game);
        $message = $this->service->getMessage();

        $return = [
            'message' => $message,
            'status' => $status,
        ];

        return new JsonResponse($return);
    }

    public function update($id)
    {
        $data = $this->getParameters();

        $game = [
            'id' => $id,
            'name' => $data['name'],
            'description' => $data['description'],
            'tags' => $data['tags']
        ];

        $status = $this->service->updateRegister($game);
        $message = $this->service->getMessage();

        $return = [
            'message' => $message,
            'status' => $status,
        ];

        return new JsonResponse($return, ($status) ? 200 : 400);
    }

    public function destroy($id)
    {
        $status = $this->service->deleteRegister($id);
        $message = $this->service->getMessage();

        $return = [
            'message' => $message,
            'status' => $status,
        ];

        return new JsonResponse($return, ($status) ? 200 : 400);
    }
}
