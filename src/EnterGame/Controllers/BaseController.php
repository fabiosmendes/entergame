<?php

namespace EnterGame\Controllers;

use EnterGame\Service\BaseService;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct(BaseService $service)
    {
        $this->service = $service;
    }

    public function getParameters()
    {
        $postVars = [];
        if($_SERVER['REQUEST_METHOD'] == 'PUT') {
            parse_str(file_get_contents("php://input"), $postVars);
        } else if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $postVars = $_POST;
        }
        return $postVars;
    }
}