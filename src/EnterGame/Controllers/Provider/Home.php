<?php

namespace EnterGame\Controllers\Provider;

use Silex\Application;
use Silex\ControllerProviderInterface;

use EnterGame\Controllers\HomeController;
use EnterGame\Service\GameService;

class Home implements ControllerProviderInterface
{
    const CONTROLLER = 'home.controller';

    public function connect(Application $app)
    {
        $home = $app['controllers_factory'];
        $this->share($app);

        $app->get('/', self::CONTROLLER . ':index');

        $app->get('/load', self::CONTROLLER . ':load');

        $home->before(function() {
            // check for something here
            echo 'Before Action';
        });

        return $home;
    }

    private function share(Application $app)
    {
        // Define controller services
        $app[self::CONTROLLER] = $app->share(function() use ($app) {
            $repository = new GameService($app['orm.em']);
            return new HomeController($repository);
        });
    }
}
