<?php

namespace EnterGame\Controllers\Provider;

use Silex\Application;
use Silex\ControllerProviderInterface;

use Symfony\Component\HttpFoundation\Request;

use EnterGame\Controllers\GameController;
use EnterGame\Service\GameService;

class Game implements ControllerProviderInterface
{
    const CONTROLLER = 'game.controller';

    public function connect(Application $app)
    {
        $users = $app['controllers_factory'];
        $this->share($app);

        $users->before(function(Request $request) {
        });

        $users->get('/', self::CONTROLLER  . ':index');
        $users->post('/', self::CONTROLLER . ':store');

        $users->get('/{id}', self::CONTROLLER . ':show')
              ->assert('id', '\d+'); // id must be digital

        $users->put('/{id}', self::CONTROLLER . ':update')
              ->assert('id', '\d+');

        $users->delete('/{id}', self::CONTROLLER . ':destroy')
              ->assert('id', '\d+');

        $users->options('/', self::CONTROLLER . ':describeContent');

        $users->after(function () {

        });

        return $users;
    }

    private function share(Application $app)
    {
        // Define controller services
        $app[self::CONTROLLER] = $app->share(function() use ($app) {
            $serviceClient = new GameService($app['orm.em']);
            return new GameController($serviceClient);
        });
    }
}