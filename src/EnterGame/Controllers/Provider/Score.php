<?php

namespace EnterGame\Controllers\Provider;

use Silex\Application;
use Silex\ControllerProviderInterface;

use Symfony\Component\HttpFoundation\Request;

use EnterGame\Service\ScoreService;
use EnterGame\Controllers\ScoreController;


class Score implements ControllerProviderInterface
{
    const CONTROLLER = 'score.controller';

    public function connect(Application $app)
    {
        $score = $app['controllers_factory'];
        $this->share($app);

        $score->before(function(Request $request) {
            echo "before";
            exit();
        });

        $score->get('/', self::CONTROLLER  . ':index');
        $score->post('/', self::CONTROLLER . ':store');

        $score->get('/{id}', self::CONTROLLER . ':show')
              ->assert('id', '\d+'); // id must be digital


        $score->after(function () {
            echo "after";
            exit();
        });

        return $score;
    }

    private function share(Application $app)
    {
        // Define controller services
        $app[self::CONTROLLER] = $app->share(function() use ($app) {
            $serviceClient = new ScoreService($app['orm.em']);
            return new ScoreController($serviceClient);
        });
    }
}