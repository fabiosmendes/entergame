<?php

namespace EnterGame\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;

class ScoreController extends BaseController
{
    public function index()
    {
        $list = $this->service->getList();
        return new JsonResponse($list, count($list) > 0 ? 200 : 400);
    }

    public function show($id)
    {
        $list = $this->service->getGameScore($id);
        return new JsonResponse($list, count($list) > 0 ? 200 : 400);
    }

    public function store()
    {
        $game = $this->getParameters();

        $game = [
            'game' => $data['game'],
            'player_nickname' => $data['player_nickname'],
            'points' => $data['points']
        ];

        $status = $this->service->newRegister($game);
        $message = $this->service->getMessage();

        $return = [
            'message' => $message,
            'status' => $status,
        ];

        return new JsonResponse($return, $status ? 200 : 400);
    }
}
