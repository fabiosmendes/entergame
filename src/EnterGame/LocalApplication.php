<?php

namespace EnterGame;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Debug\ExceptionHandler;

use EnterGame\Provider\Login\LoginBuilder;

class LocalApplication extends Application
{
    public function __construct(array $values = [])
    {
        parent::__construct($values);

        ExceptionHandler::register(true);

        LoginBuilder::mountProviderIntoApplication('/auth', $this);

        $config = new Config();
        $config->init($this);

        //handling CORS respons with right headers
        $this->after(function(Request $request, Response $response) {
            var_dump($request); exit();
            $response->headers->set('Access-Control-Allow-Origin', '*');
        });
    }
}