<?php

namespace EnterGame\Service;

use Doctrine\ORM\EntityManager;

use \EnterGame\Entities\Score as ScoreModel;

/**
 *
 * Class ScoreService
 * @package EnterGame\Service
 */
class ScoreService extends BaseService
{
    const ENTITY = 'EnterGame\Entities\Score';

    const MESSAGE_SUCCESS = 'Score registrado!'; // refactoring candidate
    const MESSAGE_FAIL = 'Não foi possível registrar o score!'; // refactoring candidate

    protected $playerService;

    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->playerService = new PlayerService($em);
    }

    public function getGameScore($id)
    {
        $scores = $this->em->getRepository(self::ENTITY)->getGameScore($id);
        $list = [];
        foreach($scores as $score) {
            $list[] = $score;
        }
        return $list;
    }

    public function getList()
    {
        $scores = $this->em->getRepository(self::ENTITY)->getAll();
        $list = [];
        foreach($scores as $score) {
           $list[] = $score;
        }
        return $list;
    }

    public function newRegister(array $data)
    {
        $this->data = $data;

        if($this->validateNewRegister()) {

            $player = $this->getPlayer();
            if($player && $this->notExistsPlayerScore($player->getId())) {
                $score = new ScoreModel();

                $score->setPlayer($player);
                $score->setPoints($this->data['points']);

                $this->em->persist($score);
                $this->em->flush();

                $this->message = self::MESSAGE_SUCCESS;
                return true;
            }

            $this->message = self::MESSAGE_FAIL;
            return false;
        }

        return false;
    }

    public function getPlayer()
    {
        $player = $this->playerService->getPlayerByNickname($this->data['player_nickname']);

        if(!$player) {
            $dataPlayer = [
                'game' => $this->data['game'],
                'nickname' => $this->data['player_nickname'],
                'avatar' => '',
                'description' => ''
            ];

            $this->playerService->newRegister($dataPlayer);
            $player = $this->playerService->getPlayer();
            if(!$player) {
                $this->message = $this->playerService->getMessage();
                return false;
            }
        }
        return $player;
    }

    /**
     * @todo move to class validate
     *
     * @return bool
     */
    public function validateNewRegister()
    {
        $validate = true;
        if($this->data['game'] == '' || $this->data['player_nickname'] == '') {
            $this->message = self::MESSAGE_FAIL;
            $validate = false;
        }

        return $validate;
    }

    public function notExistsPlayerScore($playerId)
    {
        $score = $this->getEm()->getRepository(self::ENTITY)
                      ->findOneBy([
                        'player' => $playerId,
                        'points' => $this->data['points'],
                      ]);

        if($score) {
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

}