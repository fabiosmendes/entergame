<?php

namespace EnterGame\Service;

use \EnterGame\Entities\Game as GameModel;

/**
 * @todo - refactoring name class
 *
 * Class Game
 * @package EnterGame\Service
 */
class GameService extends BaseService
{
    const ENTITY = 'EnterGame\Entities\Game';

    const MESSAGE_SUCCESS = 'Jogo cadastrado!'; // refactoring candidate
    const MESSAGE_SUCCESS_UPDATE = 'Cadastro alterado!'; // refactoring candidate
    const MESSAGE_FAIL = 'Todos os campos são obrigatórios!'; // refactoring candidate
    const MESSAGE_FAIL_UPDATE = 'Todos os campos são obrigatórios!'; // refactoring candidate
    const MESSAGE_DELETE = 'Registro excluído com sucesso!'; // refactoring candidate
    const MESSAGE_FAIL_DELETE = 'Falha ao remover registro!'; // refactoring candidate

    public function find($id)
    {
        return false;
    }

    public function getItem($id)
    {
        return $this->em->getRepository(self::ENTITY)->findOneBy(['id' => $id]);
    }

    public function getList()
    {
        $games = $this->em->getRepository(self::ENTITY)->findAll();
        $list = [];
        foreach($games as $game) {
           $list[] = $game->__toArray();
        }
        return $list;
    }

    public function newRegister(array $data)
    {
        $this->data = $data;

        if($this->validateNewRegister()) {
            $game = new GameModel();

            $game->setName($this->data['name']);
            $slug = $this->createSlug($this->data['name']);
            $game->setSlug($slug);
            $game->setDescription($this->data['description']);
            $game->setTags($this->data['tags']);

            $this->em->persist($game);
            $this->em->flush();

            $this->message = self::MESSAGE_SUCCESS;
            return true;
        }

        return false;
    }

    /**
     * @todo move to class validate
     *
     * @return bool
     */
    public function validateNewRegister()
    {
        $validate = true;
        if($this->data['name'] == '') {
            $this->message = self::MESSAGE_FAIL. ' peixe!';
            $validate = false;
        }

        return $validate;
    }

    public function updateRegister(array $data)
    {
        $this->data = $data;

        if($this->validateUpdateRegister()) {

            $game = $this->em->getReference(self::ENTITY, $this->data['id']);

            $game->setName($this->data['name']);
            $this->data['slug'] = $this->createSlug($this->data['name']);
            $game->setSlug($this->data['slug']);
            $game->setDescription($this->data['description']);
            $game->setTags($this->data['tags']);

            $this->em->persist($game);
            $this->em->flush();

            $this->message = self::MESSAGE_SUCCESS_UPDATE;
            return true;
        }

        return false;
    }

    /**
     * @todo move to class validate
     *
     * @return bool
     */
    public function validateUpdateRegister()
    {
        $validate = true;

        if($this->data['id'] == '' || $this->data['name'] == '') {
            $this->message = self::MESSAGE_FAIL_UPDATE;
            $validate = false;
        }

        return $validate;
    }

    function createSlug($str)
    {
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", '-', $clean);

        return $clean;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteRegister($id)
    {
        $game = $this->em->getRepository(self::ENTITY)
                         ->findOneBy(['id' => $id]);

        if($game) {
            $this->em->remove($game);
            $this->em->flush();

            $this->message = self::MESSAGE_DELETE;
            $validate = true;
        } else {
            $this->message = self::MESSAGE_FAIL_DELETE;
            $validate = false;
        }

        return $validate;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

}