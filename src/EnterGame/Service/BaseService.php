<?php

namespace EnterGame\Service;
use Doctrine\ORM\EntityManager;


class BaseService
{

    /**
     * Data contains info about entity
     * @var array
     */
    protected $data = [];

    /**
     * Flag for tracking status of operation
     * @var bool
     */
    protected $status = true;

    /**
     * Message from result operation
     * @var string
     */
    protected $message = '';

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param int $id
     * @return object
     */
    public function getItem($id)
    {}

    /**
     * @return array
     */
    public function getList()
    {}

    /**
     * @param array $data
     * @return bool
     */
    public function newRegister(array $data)
    {}

    /**
     * @param array $data
     * @return bool
     */
    public function updateRegister(array $data)
    {}

    /**
     * @param int $id
     * @return bool
     */
    public function deleteRegister($id)
    {}

    /**
     * @return string
     */
    public function getMessage()
    {}

}