<?php

namespace EnterGame\Service;

use \EnterGame\Entities\Player as PlayerModel;

/**
 * @todo - refactoring name class
 *
 * Class Game
 * @package EnterGame\Service
 */
class PlayerService extends BaseService
{
    const ENTITY = 'EnterGame\Entities\Player';

    const MESSAGE_SUCCESS = 'Jogador cadastrado!'; // refactoring candidate
    const MESSAGE_SUCCESS_UPDATE = 'Cadastro alterado!'; // refactoring candidate
    const MESSAGE_FAIL = 'Todos os campos são obrigatórios!'; // refactoring candidate
    const MESSAGE_FAIL_UPDATE = 'Todos os campos são obrigatórios!'; // refactoring candidate
    const MESSAGE_DELETE = 'Registro excluído com sucesso!'; // refactoring candidate
    const MESSAGE_FAIL_DELETE = 'Falha ao remover registro!'; // refactoring candidate

    /**
     * @var \EnterGame\Entities\Player;
     */
    protected $player;

    public function getItem($id)
    {
        return $this->em->getRepository(self::ENTITY)->findOneBy(['id' => $id]);
    }

    public function getList()
    {
        $players = $this->em->getRepository(self::ENTITY)->findAll();
        $list = [];
        foreach($players as $player) {
           $list[] = $player->__toArray();
        }
        return $list;
    }

    public function newRegister(array $data)
    {
        $this->data = $data;

        if($this->validateNewRegister()) {
            $this->player = new PlayerModel();

            $this->player->setNickname($this->data['nickname']);
            $this->player->setAvatar($this->data['avatar']);
            $this->player->setDescription($this->data['description']);
            $game = $this->getEm()->getReference('\\EnterGame\\Entities\\Game', $this->data['game']);
            $this->player->setGame($game);

            $this->em->persist($this->player);
            $this->em->flush();

            $this->message = self::MESSAGE_SUCCESS;
            return true;
        }

        return false;
    }

    /**
     * @todo move to class validate
     *
     * @return bool
     */
    public function validateNewRegister()
    {
        $validate = true;
        if($this->data['nickname'] == '' || $this->data['game'] == '' ) {
            $this->message = self::MESSAGE_FAIL;
            $validate = false;
        }

        return $validate;
    }

    public function updateRegister(array $data)
    {
        $this->data = $data;

        if($this->validateUpdateRegister()) {

            $this->player= $this->em->getReference(self::ENTITY, $this->data['id']);

            $this->player->setNickname($this->data['nickname']);
            $this->player->setAvatar($this->data['avatar']);
            $this->player->setDescription($this->data['description']);
            $game = $this->getEm()->getReference('\\EnterGame\\Entities\\Game', $this->data['game']);
            $this->player->setGame($game);

            $this->em->persist($this->player);
            $this->em->flush();

            $this->message = self::MESSAGE_SUCCESS_UPDATE;
            return true;
        }

        return false;
    }

    /**
     * @todo move to class validate
     *
     * @return bool
     */
    public function validateUpdateRegister()
    {
        $validate = true;

        if($this->data['id'] == '' || $this->data['nickname'] == '' || $this->data['game'] == '' ) {
            $this->message = self::MESSAGE_FAIL_UPDATE;
            $validate = false;
        }

        return $validate;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteRegister($id)
    {
        $game = $this->em->getRepository(self::ENTITY)
                         ->findOneBy(['id' => $id]);

        if($game) {
            $this->em->remove($game);
            $this->em->flush();

            $this->message = self::MESSAGE_DELETE;
            $validate = true;
        } else {
            $this->message = self::MESSAGE_FAIL_DELETE;
            $validate = false;
        }

        return $validate;
    }

    public function getPlayerByNickname($nickname)
    {
        return $this->getEm()->getRepository(self::ENTITY)
                        ->findOneBy(['nickname' => $nickname]);
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Catch object Player of the last operation. Ex: newRegister or  updateRegister
     *
     * @return PlayerModel
     */
    public function getPlayer()
    {
        return $this->player;
    }
}