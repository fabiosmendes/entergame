
App = function() {

        var access = this;
        var urlRest = 'http://localhost:8080';
        var authorizationToken = '123456789';

        var SECTION_LIST = '#list';
        var SECTION_FORM = '#section-form';
        var TABLE_HEADER;

        this.init = function () {

            access.requestList();

            // @todo - block screen in first request
            $(function () {
                $('#add_client').bind('click', access.goToForm);
                $('#btn_back').bind('click', access.goToList);
                $('#btn_save').bind('click', access.submitForm);
            });

        };

        this.displayLoading = function () {
            $('#list').hide();
            $('#section-form').hide();
            $('#loading').fadeIn();
        };

        this.hideLoadingShowId = function (displayId) {
            $('#loading').hide();
            $(displayId).show();
        };

        this.requestList = function () {

            access.displayLoading();
            this.clearTable();

            $.ajax({
                type: "GET",
                dataType: "json",
                url: urlRest + "/clients/",
                beforeSend: function (request) {
                    request.setRequestHeader("Authority", authorizationToken);
                },
                success: function (data) {

                    $(".table-list").append(TABLE_HEADER);
                    $(data).each(function (key, obj) {
                        var id = obj.id;
                        var viewLink = '<a href="#" class="view_item " data-id="' + id + '" >Visualizar</a>';
                        var deleteLink = '<a href="#" class="delete_item alert-link" data-id="' + id + '">Excluir</a>';

                        $(".table-list").append($('<tr/>')
                            .append($('<td/>').html(obj.first_name))
                            .append($('<td/>').html(obj.last_name))
                            .append($('<td/>').html(obj.address))
                            .append($('<td/>').html(viewLink + " - " + deleteLink))
                        );
                    });

                    $('.view_item').each(function() {
                        $(this).bind('click', function() {
                            return app.viewItem($(this).attr('data-id'));
                        });
                    });

                    $('.delete_item').each(function() {
                        $(this).bind('click', function() {
                            return app.confirmDeleteItem($(this).attr('data-id'));
                        });
                    });

                    access.hideLoadingShowId(SECTION_LIST);
                },
                error: function (data, req) {
                    // @todo - display error
                }
            });
        };

        this.clearTable = function() {
            TABLE_HEADER = $(".table-list").find('tr').first().clone();
            $(".table-list").html('');
        }

        this.goToForm = function (e) {
            e.preventDefault();

            $('#list').hide();
            access.hideMessageForm();

            $('#form_user').find('input').each(function() {
                if($(this).attr('type') != 'button' && $(this).attr('type') != 'submit') {
                    $(this).val('');
                }
            });

            $('#section-form').fadeIn();
        };

        this.goToList = function (e) {
            e.preventDefault();

            $('#section-form').hide();
            $('#list').fadeIn();
        };

        this.viewItem = function (id) {

            access.displayLoading();
            $.ajax({
                type: "GET",
                dataType: "json",
                url: urlRest + "/clients/" + id,
                success: function (data) {
                    if(data.status) {
                        var obj = data.object;

                        $('#id').val(obj.id);
                        $('#first_name').val(obj.first_name);
                        $('#last_name').val(obj.last_name);
                        $('#address').val(obj.address);
                    }

                    access.hideLoadingShowId(SECTION_FORM);
                },
                error: function (data, req) {
                    // @todo - implement display error
                }
            });

            return false;
        };

        this.confirmDeleteItem = function (id) {

            if(confirm('Deseja excluir esse item?')) {
                access.displayLoading();
                access.deleteItem(id);
            }

            return false;
        };

        this.deleteItem = function (id) {
            access.displayLoading();
            $.ajax({
                type: "DELETE",
                url: urlRest + "/clients/" + id,
                success: function (data) {
                    if(data.status) {
                        access.displayMessageSuccessForm(data.message);
                        access.requestList();
                    } else {
                        access.displayMessageErrorForm(data.message);
                    }

                },
                error: function (data, req) {
                    // @todo - implement display error
                }
            });

            return false;
        };

        this.submitForm = function (e) {
            e.preventDefault();

            if ($('#id').val() == '') {
                access.storeItem();
            } else {
                access.updateItem();
            }
        };

        this.storeItem = function () {
            access.hideMessageForm();
            var formData = $('#form_user').serialize();
            access.displayLoading();
            $.ajax({
                type: "POST",
                dataType: "json",
                data: formData,
                crossDomain: true,
                url: urlRest + "/clients/",
                success: function (data) {
                    if (!data.status) {
                        access.displayMessageErrorForm(data.message);
                        access.hideLoadingShowId(SECTION_FORM);
                    } else {
                        access.displayMessageSuccessForm(data.message);
                        access.requestList();
                    }
                    return false;

                },
                error: function (data, req) {
                    // @todo - implement display error
                }
            });

            return false;
        };

        this.updateItem = function () {
            access.hideMessageForm();
            var formData = $('#form_user').serialize();
            var id = $('#id').val();
            access.displayLoading();
            $.ajax({
                type: "PUT",
                dataType: "json",
                data: formData,
                url: urlRest + "/clients/" + id,
                success: function (data) {
                    if(data.status) {
                        $('#id').val(data.id);
                        $('#first_name').val(data.first_name);
                        $('#last_name').val(data.last_name);
                        $('#address').val(data.address);

                        access.displayMessageSuccessForm(data.message);
                        access.requestList();
                    } else {
                        access.displayMessageErrorForm(data.message);
                        access.hideLoadingShowId(SECTION_FORM);
                    }
                },
                error: function (data, req) {
                    // @todo - display error
                }
            });
        };

        this.hideMessageForm = function () {
            var messageElement = $('#message');
            messageElement.removeClass('alert-danger');
            messageElement.removeClass('alert-success');
            messageElement.hide();
        };

        this.displayMessageErrorForm = function (message) {
            var messageElement = $('#message');
            messageElement.html(message);
            messageElement.addClass('alert-danger');
            messageElement.show();
            setTimeout(access.hideMessageForm, 5000);
        };

        this.displayMessageSuccessForm = function (message) {
            var messageElement = $('#message');
            messageElement.html(message);
            messageElement.addClass('alert-success');
            messageElement.show();
            setTimeout(access.hideMessageForm, 5000);

        };
};

var app = new App();
app.init();