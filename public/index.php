<?php

// based on article: http://www.sitepoint.com/introduction-silex-symfony-micro-framework/

require_once __DIR__ . '/../vendor/autoload.php';

$app = new \EnterGame\LocalApplication();
$app->run();